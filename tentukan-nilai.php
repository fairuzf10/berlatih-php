<?php
function tentukan_nilai($number)
{
    //  kode disini

    $keluaran = ""; 
    if($number>=85 && $number<=100)
    {
        $keluaran .= "Sangat Baik <br>";
    }

    else if($number>=70 && $number < 85)
    {

            $keluaran .= "Baik <br>"; 
    }

    else if($number>=60 && $number<70)
    {
        $keluaran .= "Cukup <br>"; 
    }
    else 
    {
        $keluaran .= "Kurang <br>"; 
    }

    return $keluaran; 

}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>